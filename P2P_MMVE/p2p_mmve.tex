\graphicspath{{p2p_mmve/fig/}}

\chapter{Peer-to-Peer MMVE state consistency and persistence models} \label{chap:p2p_mmve}

Previous chapters provide a general introduction to massively multi-user virtual environments, peer-to-peer systems and peer-to-peer massively multi-user virtual environments. Furthermore chapter \ref{chap:state_consistency} provides additional information on state consistency and the key challenges and requirements of P2P MMVEs. This chapter provides more insight into the proposed Pithos architecture, by introducing various persistence models, commonly used in MMVEs and P2P MMVEs.

\section{Object Consistency}

As previously introduced in section \ref{sec:ve}, VE objects cab typically be categorised into four object types, namely \textit{immutable objects}, \textit{mutable objects}, \textit{player characters/avatars} and \textit{Non-player Characters} (NPCs). For the interest of state consistency and persistence, all four object types are important. However, as mutable objects are crucial to user interaction and game logic, state consistency for these objects is of higher importance. Mutable objects occur in two forms within a VE, the root form, i.e. the original object, and the replicated form, which are copies of the original object. Replicas are used to achieve low-latency, system wide consistency. Object replicas are copied to each user, interested in the object, in order to process interactions locally. These local object state changes and interactions are used for display purposes \cite{p2p_architectures}.
In chapter \ref{chap:state_consistency}, definitions of state management and state persistence, sub-domains of a consistency architecture are provided. It is worth noting that the state persistence model is determined by the high-level consistency model used. The following sections will describe different consistency models used in modern MMVEs.

\section{Classic consistency models}

In order to provide more insight into the proposed architecture, this section will introduce the classic consistency models that are commonly used in MMVEs. These consistency models, provide the fundamental building blocks that almost all other consistency models use in some shaper or form.

\subsection{Event-based}

The event-based consistency model, also known as the \textit{fully-distributed} consistency model, dictates that each peer stores the complete game state. For object updates, each peer preforms object updates on its local object replicas, these updates are then propagated to all other peers within the network. This approach does however require a high degree of coordination, as all peers need to receive object updates in the same order. If the order of updates are inconsistent, world state will become inconsistent. Various mechanisms have been implemented to ensure update order, like lockstep techniques \cite{lockstep} or event deadlines \cite{p2p_survey}.
The \textit{persistence model} of an even-based consistency model is that all peers store the complete game state. Figure \ref{fig:event-based} illustrates the event-based model.

\begin{itemize}
  \item [\textbf{Advantages:}]
  \item Responsive, as this consistency model is designed for low latency.
  \item Distributed computation.
  \item [\textbf{Disadvantages:}]
  \item Does not scale well, traffic increases with factor $N^2$, Where $N$ is the number of peers.
  \item Lockstep techniques increase latency to twice that of the slowest peer.
  \item Resource intensive, with high bandwidth usage.\\
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.40]{event-based.png}
  \caption[Event-based consistency model]{
  Event-based consistency model \cite{p2p_survey}.
  }
  \label{fig:event-based}
\end{figure}

\subsection{Update-based}

Contrary to the event-based model, the \textit{update-based} model uses a C/S network architecture. The server contains most of the game logic and stores the authoritative game state, which all clients must conform to. Clients store a non-authoritative view of the game state. Clients send updates to the server, that in turn updates the global object state and informs all clients of the altered object state. Clients are responsible for keeping their game state in sync with the server's authoritative game state. It can therefore be stated that clients primarily use their non-authoritative game state for reponsive interaction and display purposes. \cite{p2p_survey}
The update-based \textit{persistence model} dictates that the authoritative game state is stored on a centralised server, whereas clients store a non-authoritative game state. Figure \ref{fig:update-based} illustrates the update-based model.

\begin{itemize}
  \item [\textbf{Advantages:}]
  \item Security is easy to implement and enforce.
  \item More scalable than a fully-distributed model.
  \item Minimal client-side game logic required.
  \item Can scales vertically and Horizontally.
  \item [\textbf{Disadvantages:}]
  \item Requires powerful servers to act as authoritative nodes.
  \item Less responsive due to centralised approach.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.40]{update-based.png}
  \caption[Update-based consistency model]{
  Update-based consistency model \cite{p2p_survey}.
  }
  \label{fig:update-based}
\end{figure}

\section{Client/Multiserver Consistency Models}

In order to provide more insight into the proposed architecture, this section will introduce various classic storage models, that are commonly used in C/MS MMVEs.

\subsection{Sharding}

Most popular MMVEs to date, make use of some sort of C/S architecture. One of the most common C/S architectures for MMVEs is the Distributed multi-server architecture, where multiple servers are geographically distributed. Typically these servers host different instances of the same world, also referred to as \textit{shards}. This storage model is referred to as \textit{sharding} \cite{p2p_architectures} \cite{p2p_survey}. Shards are completely standalone instances and are therefore only responsible for state consistency and management of its VE. Users can typically only interact with other users in the same shard. Figure \ref{fig:sharding} provides illustrates a sharded storage model.

\begin{itemize}[$\bullet$]
  \item [\textbf{Advantages:}]
  \item Improved scalability, compared to single server approach, and can be scaled horizontally as load increases.
  \item Security and administration is easy to implement.
  \item Duplicate worlds mean more users can be hosted, without hitting server limitation.
  \item System maintenance can be distributed amongst regions, leading to higher availability.
  \item [\textbf{Disadvantages:}]
  \item Users can crowd certain shards, making them slow and unresponsive.
  \item Users cannot communicate or interact with users on other shards.
  \item Some shards might be empty, leading to wasted resources.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.60]{sharding.png}
  \caption[Sharding consistency model]{
  Sharded consistency model. \cite{p2p_architectures}
  }
  \label{fig:sharding}
\end{figure}

\subsection{Region-based}

Another popular persistence model, is a \textit{region-based} storage model, where a single world is split into multiple regions. Each region is hosted on a separate server, to distribute the load amongst multiple servers \cite{p2p_architectures}, \cite{p2p_survey}. Users are able to interact with users from other regions, however this requires a \textit{hand-off} mechanism, that effectively migrates a user to another server. In MMOGs this typically requires a user to travel through an in-game "portal". Figure \ref{fig:zone} provides illustrates a region-based storage model.

\begin{itemize}
  \item [\textbf{Advantages:}]
  \item Improved scalability, compared to a sharded approach, and can be scaled horizontally as load increases.
  \item Users close to one another in the VE, are likely to be hosted on the same server, ensuring higher quality interactions.
  \item Regional server only store objects within its own region, thus less resources are required.
  \item Improved load balancing.
  \item Regions can be used for interest management in the VE.
  \item [\textbf{Disadvantages:}]
  \item Users can crowd certain regions, making them slow and unresponsive.
  \item Some regions might be empty, leading to wasted resources.
  \item The system requires a hand-off mechanism for migration between regions.
  \item Hand-off mechanism might not be transparent for users.
  \item Increased system complexity.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.60]{zone.png}
  \caption[Region-based consistency model]{
  Region-based consistency model \cite{p2p_architectures}.
  }
  \label{fig:zone}
\end{figure}

\subsection{Replication-based}

A \textit{replication-based} storage model is also a commonly used storage approach. This model is similar to sharding, as multiple instances of the world exist. In Contrast to sharding, the replication-based model aims to create a single consistent "view" of the VE. This model therefore has a strict consistency requirement between servers. Consistency is ensured with the use of high quality link between servers \cite{pithos_phd}, \cite{p2p_survey}. Users are able to interact with any other user in the VE. Figure \ref{fig:replication} provides illustrates a replication-based storage model.\\

\begin{itemize}
  \item [\textbf{Advantages:}]
  \item Users can interact and communicate freely within the VE.
  \item Provides a consistent view of the world for all users.
  \item Achieves a high degree of availability.
  \item Can be easily scaled horizontally.
  \item [\textbf{Disadvantages:}]
  \item Worlds are replicated, requiring huge amounts of resources.
  \item Unpredictable user experience, as two users interacting with one another may be hosted on different servers.
  \item Eventual consistency.
  \item Increased complexity required to maintain consistency.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.60]{replication.png}
  \caption[Replication-based consistency model]{
  Replication-based consistency model \cite{p2p_architectures}.
  }
  \label{fig:replication}
\end{figure}

\section{Peer-to-peer MMVE consistency models} \label{sec:p2p_consistency}

\subsection{Region-based Update-based consistency model}

As stated previously, most P2P MMVE consistency models are based on the classic event-based or update-based consistency models. One such consistency model is the \textit{region-based updated-based} consistency model, which makes use of the classic update-based consistency model, combined with the region-based model. It might seem like an event-based (fully-distributed) model might be more suited in a P2P MMVE consistency model, however, due to lack of research interest and scaling difficulties the event-based consistency model is not suitable for P2P MMVEs \cite{p2p_survey}.

The region-based consistency model is applied to the P2P MMVE by segmenting the world into separate regions, where each region is "hosted" by a super-peer, which acts as the authoritative server. A super-peer in this context, is a special type of peer, with a specific set of responsibilities, typically administrative. Each super-peer receives all events, hosts game logic, distributes object updates and is responsible for facilitating the consistency architecture i.e. state management and persistence. In contrast, normal peers merely store their local game state and some client logic to allow local object updates for display purposes.

The update-based consistency architecture is applied to P2P MMVEs by introducing authoritative objects. Authoritative objects are distributed amongst super-peers in the system. Each super-peer becomes the authoritative node for a set of authoritative objects, following the update-based consistency model. Super-peers receive all update events for the authoritative objects they store and accordingly propagates all state updates to other peers \cite{p2p_architectures}, \cite{p2p_survey}.

Figure \ref{fig:ubrb} illustrates the region-based update-based P2P MMVE consistency model.

\begin{itemize}
  \item [\textbf{Advantages:}]
  \item Does not require powerful servers.
  \item Regions can be used for interest management in the VE.
  \item Regional server only store objects within its own region, thus less resources are required.
  \item More scalable than a fully-distributed model.
  \item Minimal client-side game logic required.
  \item [\textbf{Disadvantages:}]
  \item Increased system complexity.
  \item Users can crowd certain regions, making them slow and unresponsive.
  \item Super-peers are required to contribute more resources to the network than normal peers.
  \item Bad super-peers can cause reduce game performance and user experience.
  \item Reduced security as all region data is stored on one peer.
  \item Constant network churn, means super-peers leaving the network, might cause regions to become unavailable.
  \item The system requires a hand-off mechanism for migration between regions.
\end{itemize}

\begin{figure}[H]
  \includegraphics[scale=0.25]{UbRb.png}
  \caption[Region-based Update-based P2P MMVE consistency model]{
  Region-based Update-based P2P MMVE consistency model \cite{p2p_architectures}
  }
  \label{fig:ubrb}
\end{figure}

\section{Peer-to-peer MMVE persistence models}

In the previous section, an overview of classic and C/MS consistency models are provided. Classic and C/MS MMVE storage models rely heavily on centralised servers, which in turn need to be extremely powerful, have large databases and high quality data links. These storage models therefore do not fit into the P2P paradigm \cite{p2p_survey}.

This section will introduce various P2P storage models, that can be used in P2P MMVEs. Each consistency model will be measured against the key P2P MMVE storage requirements identified in chapter \ref{chap:state_consistency}. This overview of available P2P MMVE storage models will also provide valuable insight into some of Pithos' design decisions.

\subsection{Super-peer storage}

The super-peer storage uses region-based consistency model, described in section \ref{sec:p2p_consistency}. Super-peer storage requires a super-peer selection strategy, that will ensure a peer with enough resources and a good reputation is selected (promoted) to become a super peer \cite{p2p_survey}.

\subsubsection{Fairness}

Super-peers storage is not considered fair, seeing as super-peers are required to contribute a lot more resources to the network than normal peers. Region overloading is a serious concern for region-based MMVEs, and seeing as a super-peer could easily be overloaded, the storage type can't be considered fair.

\subsubsection{Reliability}

P2P environments experience high network churn. This adds an additional redundancy requirement to super-peer storage, to ensure that even if a super-peer leaves the network, the region will still function as expected. One solution to ensure reliability is to allocate redundant super-peers to a region, which is able to take over the server role if the main super-peer leaves the network. Super-peers and backup super-peers are required to have identical VE states.
Another way to ensure reliability is to introduce super-peer selection mechanism that select super-peers based on resources and reputability. If the mentioned additions are made to a super-peer storage system, it can be deemed to be reliable \cite{p2p_survey}.

\subsubsection{Security}

One of the weaknesses of super-peer storage is security. As mentioned in section \ref{sec:p2p_consistency}, super-peers control the entire environment state. This means if a super-peer alters objects in align malicious manner, it will be impossible to detect.
Multiple security mechanisms can be placed to ensure security. One such security mechanism is the use of a certification mechanism, to identify users. Another security mechanism that can be implemented is to ensure that all super-peers (main and backup) perform the same object updates. The hash of all object updates can be compared to ensure consistency \cite{p2p_survey}.

\subsubsection{Responsiveness}

Super-peer storage is responsive, as super-peers ensure low latency storage and retrieval operations. Generally the speed of each operation is determined by the quality of link between a client and a super peer, as no intermediate connections are required (single hop). Super-peers can however become overloaded, leading to a lower responsiveness \cite{p2p_survey}.

\subsubsection{Scalability}

Super-peer storage is deemed scalable, as regions can be segmented to such a degree to ensure computational and storage distribution. Regions are generally predetermined and is therefore only scales up to a point, unless dynamic region selection mechanisms are used. Dynamic region selection, segments an existing region if a certain number of clients populate the region. This mechanism also solves the previously discussed problem of region crowding.

\subsection{Overlay storage} \label{sec:overlay}

For the purpose of this work, overlay storage refers to a structure P2P overlay as storage network. Section \ref{sec:overlay_intro} provides a brief introduction to structured overlays. Overlay storage as a P2P persistence layer is justifiable, since any P2P distributed storage network, can be used to store only VE objects \cite{pithos_phd}.

\subsubsection{Fairness}

Structured overlays use an identifier space to allocate a unique ID to peer. This same identifier space is used to allocate IDs to objects stored in the overlay. Objects are stored on the peer with the closest match (distance-based) to its own ID. The identifier space is distributed in a random fashion, which means overlay storage can be deemed fair \cite{structured_P2P}.

\subsubsection{Reliability}

To ensure reliability in overlay storage, redundancy mechanisms can be implemented. Object replication is one o often mechanisms often used to ensure redundancy, fault-tolerance and high availability in the overlay \cite{structured_overlays}. The presence of object replicas increases the probability that a set number replicas of an object, will always be available. In the case a peer storing an original object leaves the network, the distance-based routing mechanism will ensure that traffic is automatically routed to the neighbouring peer that stores a replica of that object \cite{p2p_survey}. With the addition of redundancy, overlay storage is deemed reliable.

\subsubsection{Security}

Overlay storage is generally secure, due the distributed nature of the system. To improve security redundancy and quorum techniques can be implemented. Object replication, ensures that more than one copy of an object exists, across multiple peers. A quorum mechanism can then be used on object retrieval to request a set number of replicas, to determine if received objects are identical. It is however argued that security and trust is still challenge in for overlay storage implementations like DHTs\cite{structured_overlays}, and should be used in conjunction with other security mechanisms.

\subsubsection{Responsiveness}

Responsiveness is one of the weaknesses of overlay storage, which is caused by the computational overhead for storage and retrieval requests. Overlay storage networks are not fully connected and therefor on average require more than one hop to retrieve or store data. On average $\mathcal{O}(log N)$ hops are required to complete requests \cite{p2p_survey}.

\subsubsection{Scalability}

Structured overlay storage is considered to be sufficiently scalable, since peers and objects are normally distributed within an identifier space it achieves logarithmic scalability \cite{p2p_survey}.

\subsection{Distance-based storage}

With a distance-based storage approach, objects are stored on peers that are geographically close to the object in the virtual world. A distance metric usually determines on which specific peer an object should be stored. Voronoi storage \cite{Voronoi} is an example of a distance based storage approach. \cite{p2p_survey}

\subsubsection{Fairness}

Distance-based storage is known to be relatively fair, since objects are relatively evenly distributed amongst peers within the VE. There are however similar concerns to region-based storage, where some peers might be close to a large number of objects, effectively overloading a peer. This reduces the fairness of distance-based storage \cite{p2p_survey}. Certain mechanisms like peer \textit{overload}, \textit{underload} detectors and \textit{aggregotors}, can be put in place that can determine a peers current load and take over the responsibility of overloaded peers. These measures reduce the probability of peer overloading \cite{Voronoi}.

\subsubsection{Reliability}

Distance-based storage has some reliability concerns, due to its weakness against network churn. Similarly to region based storage, redundancy in the form of replication is used to improve reliability. Objects can be stored on multiple peers that are geographically close to the object. Object replication therefore ensures high availability of objects \cite{p2p_survey}.

\subsubsection{Security}

Similarly to region-based storage, the main concern of distance-base storage is security. Malicious peers are very difficult to detect within the network. Redundancy  in the form of replication, allows for the use of quorum mechanisms which improve the security of distance-based systems. Quorum mechanisms do however increase resource usage and reduces responsiveness \cite{p2p_survey}.
Certification mechanisms or reputation mechanism are another well known security implementations that can be used to ensure only authorized peers contribute to object storage, reducing the likelihood of malicious peers.

\subsubsection{Responsiveness}

Distance-based storage can be very responsive, when a user interacts with an object, the probability of the object being stored locally is quite high. There are however cases when multiple peers interact with the same object, which has two possible outcomes. Best case scenario, all peers store the object, in which case every peer hosting the object becomes a server for that object. Worst case, a single peer stores the object and becomes the server for the object, which still only requires a single hop to serve requests \cite{p2p_survey}.
One challenge introduced by distance based storage is that if the authoritative object store of the object changes too frequently, distance based storage becomes unresponsive. In distance based storage an object's authoritative store can change if the peer hosting the object is deemed to have left the object's Area of Interest (AoI).

\subsubsection{Scalability}

The scalability of distance based storage depends heavily on the use case, as for large VEs with few peers, a peers might be required to host many objects. with more peers joining the network, storage might become more responsive, but if peers are constantly moving, the overhead required to update the authoritative object store of an object, will reduce responsiveness. It can therefore be said that distance based storage does not scale well.

\section{Conclusion}

In this chapter various consistency models were discussed and evaluated. As an introduction, the definitions of two classic consistency models, \textit{event-based consistency} and \textit{update-based consistency}, were provided and discussed.

These classic consistency models were then expanded on with the introduction of C/MS consistency models like, \textit{sharding}, \textit{region-based consistency} and \textit{replication-based consistency}. Of the three consistency models, region-based consistency is the most promising, as it addresses many of the challenges and disadvantages of sharding and replication-based consistency models.

Continuing to expand on the fundamental consistency models, a P2P MMVE consistency model was introduced, namely, \textit{region-based update-based consistency}. This consistency model is specifically designed for P2P systems and leverages off of the strengths of both region-based and update-based consistency models, to form a highly responsive, reliable, secure and scalable consistency model for P2P MMVE storage.

Finally various P2P MMVE persistence (Storage) models were introduced, namely, \textit{super-peer storage}, \textit{overlay storage} and \textit{distance-based storage}. All three these storage types are suitable for P2P MMVEs, depending on use-case. Super-peer storage is scalable and responsive yet are not considered fair and requires additional mechanisms to ensure security and reliability. Overlay storage is highly scalable, fair and secure, yet less responsive and require additional mechanisms to ensure reliability. Distance-base storage is responsive but requires additional mechanisms to ensure reliability, security and fairness. Distance-based storage also has scalability issues.

The review of these consistency models and storage types, enabled the authors of the Pithos research paper \cite{pithos_phd}, to design a novel architecture, Pithos, which makes use of a hybrid consistency model to meet all P2P MMVE storage requirements. The next chapter provides an overview of the Pithos architecture.

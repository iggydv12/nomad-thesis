\graphicspath{{conclusion/fig/}}

\chapter{Conclusion and Recommendations} \label{chap:conclusion}

The objective of this work was to design, implement and evaluate a peer-to-peer (P2P) storage network, for state management and persistence (SMP) of a P2P MMVE.

This chapter presents a summary of the work done in this project and highlights its contributions. Recommendations are also made for future work which will allow Nomad to be used in internet facing systems.

\section{Conclusion}

Section \ref{sec:p2p_storage_req} set out the key storage requirements of P2P MMVE systems as responsiveness, reliability, security, fairness, and scalability. In light of the evaluation of Nomad described in Chapter \ref{chap:nomad_evaluation}, Pithos' simulated results can now be verified in terms of these storage requirements.

\subsection{Responsiveness}

Nomad achieves responsiveness by splitting the network into fully connected groups of peers. The underlying architecture uses a distance-based group-based storage mechanism to store, retrieve, distribute and replicate objects. The assumption is made that peers are more likely to request objects from their own group, which helps Nomad to achieve highly responsive storage and retrieval for in-group requests.

Nomad's evaluation confirmed that group storage is extremely responsive in performing storage
and retrieval operations. In terms of object storage, fast mode was found to be the most responsive
and to require the lowest amount of bandwidth compared to safe storage. In terms of object retrieval,
parallel retrieval was found to be more responsive than fast and safe retrieval, however required
more bandwidth than fast retrieval.

Creating object replicas proved to be costly in terms of resource usage, although it does improve the responsiveness of storage and retrieval requests as some peers may be geographically closer than others, leading to lower RTLs. In parallel retrieval and fast storage mode, Nomad sends identical requests to multiple peers and only awaits the first successful response, thereby taking advantage of the distribute nature of object replicas.

In terms of responsiveness, Nomad's evaluation produced similar values to those measured during Pithos' simulation. It can therefore be concluded that Pithos' measured response times are accurate and that Pithos satisfies the P2P MMVE storage requirement of responsiveness.

\subsection{Reliability}

Reliability is achieved in Nomad by making use of various modules and mechanisms. An overlay storage component in the form of a DHT increases reliability, as it is able to serve any in-group or out-of-group retrieval request. Object replication ensures that sufficient replicas exist in case of storage peers leaving the network. Replicas are maintained by object repair mechanisms, which ensures a set number of object replicas are always available.

Nomad's evaluation confirmed that the overlay storage does impact the overall performance of the network. During Nomad's evaluation, a Kademlia based DHT was used as overlay component, whereas during the Pithos simulation a Chord based DHT was used. Kademlia proved to be far less reliable and responsive compared to Chord, and led to a lower overall reliability. This proves that overlay storage does impact the overall performance of the network, and that it is required to ensure system reliability under network churn and group migration.

Nomad's overlay component was far less responsive under network churn compared to group storage. Object storage requests were reliable, but retrieval requests far less so, and also less responsive. Nomad can be described as less reliable than the Pithos simulation, as out-of-group requests are likely to fail in Nomad. As such, Nomad's overlay storage is one of the system's weaknesses and requires further investigation.

The use of object replicas improved overall reliability, as replication provides high availability for objects in the presence of group migration and network churn. Nomad's scheduled and leave-repair mechanisms proved to consistently maintain object replicas, but had a significant impact on performance.

Nomad's evaluation provided similar results to those of the Pithos simulation. It can therefore be concluded that Pithos satisfies the P2P MMVE storage requirement of reliability.

\subsection{Security}

Security is achieved in Nomad by making use of object replication and a quorum mechanism. Nomad also supports \textit{safe} reads and writes, which inherently increases reliability and security.

Object replication diminishes the impact of malicious peers in an MMVE. As multiple peers store object replicas, a maliciously altered version of the object is easily identifiable. Multiple object replicas also enable the use of quorum mechanisms on parallel responses.

When parallel requests are made, depending on the retrieval mode, a quorum mechanism can be used to verify object state. This way, a peer can decide to select only the most frequently occurring version of an object. This effectively diminishes the impact of malicious peers in the system and improves security. Nomad's quorum mechanism can be described by the quorum formula $(\mathcal{R}/2) + 1$, where $\mathcal{R}$ is the replication factor.

During Nomad's evaluation, it was proven that different retrieval modes have varying degrees of success in identifying maliciously altered objects.  Safe storage is the most secure retrieval mode, as multiple retrieval responses are compared before a decision is made on the integrity of an object. Fast and parallel retrieval are less secure, as the first response is always sent to the higher layer.

Pithos implements a certificate authority (CA) and a certification mechanism, in order to assign IDs and certificates to peers, and to sign object changes. Nomad does not make use of this certification mechanism and therefore, in its current state does not support Secure Sockets Layer (SSL) communication between peers or signing of object changes. These certification mechanisms are considered future work.

Nomad's implementation lacks the security features of the Pithos architecture, but does provide core security functionality for verifying object integrity. Nomad's evaluation proved that Pithos' results are accurate and that Pithos satisfies the P2P MMVE storage requirement of security.

\subsection{Fairness}

To ensure fairness (load-balancing), peers and super-peers are not selected on any discriminating factors but purely based on their location and time of joining the group. Load is distributed evenly across all peers in the network by making use of randomly distributing group storage requests. Group storage and overlay storage are inherently considered to be fair.

The group storage mechanism distributes object replicas in a uniformly random fashion, ensuring that no one peer is favoured for storage purposes. Peers use their own copy of the peer ledger to choose random peers in the group when (a) requesting objects or (b) storing object replicas.

For overlay storage, a DHT is used. DHTs map objects and peers to the same identifier space. Objects are stored on peers with the closest ID match. This way, if IDs are assigned in a uniformly random fashion, storage load is inherently distributed in a uniformly random fashion.

During Nomad's evaluation, fairness was not directly measured, but observation of resource usage confirmed that all peers in a group had similar memory footprints. From Nomad's evaluation, it can be confirmed that objects were evenly distributed amongst peers. This means that Pithos' measured object distribution results can be considered accurate and that Pithos satisfies the P2P MMVE storage requirement of fairness.

\subsection{Scalability}

In Nomad scalability, is achieved by making use of modules that are scalable in themselves. Nomad uses either a size-based or Voronoi-based grouping mechanism, which splits the network into fully connected groups. Groups have a fixed size limit, as fully connected networks scale quadratically in terms of messages required per transaction. Group size is therefore the limiting scaling factor. For its overlay storage component, Nomad makes use of a DHT, which is by design scalable.

One possible system bottleneck is the directory server, since it facilitates network bootstrapping, and provides peers with a local group cache which is used for various purposes. For Nomad's directory server, a scalable and highly consistent distributed coordination system called ZooKeeper is used. ZooKeeper can be scaled vertically and horizontally, and is already provided by many cloud providers as a fully configured product.

Nomad's evaluation proved that as the number of nodes in the network increased, performance was not severely affected. Since Nomad could only be evaluated with up to 50 peers, it is recommended that a scalability evaluation be executed using a higher number of nodes.

\section{Summary of Work}

This project presented the development and evaluation of a Pithos based decentralised storage network, created specifically for P2P MMVEs, that could be used to verify Pithos' simulated results. A standalone Java based application, called Nomad, was created using various frameworks and technologies, in order to satisfy the component and use case requirements of the Pithos architecture. The implemented application was deployed to multiple machines and evaluated under Normal network conditions.

\subsection{Nomad Design and Implementation}

The use case of a Pithos based system, is that of a generic storage system, supporting object storage, retrieval, modification and removal. Nomad implements all Pithos' required modules and mechanisms, in order to satisfy the basic system use cases of a storage network, whilst still adhering to the P2P MMVE storage requirements of reliability, responsiveness, scalability, security and fairness.

Various alterations and adjustments were made to the original architecture, in order to satisfy the maintainability requirements of a real-world implementation, whilst dealing with language specific restrictions.

\subsection{Nomad Evaluation}

The responsiveness, reliability and security of Nomad were evaluated, taking bandwidth usage into account. Nomad's performance was compared to Pithos' simulated performance so as to verify the integrity of both the architecture and the simulation.

Results provided by various experiments proved that Nomad is a reliable, responsive, scalable and secure decentralised storage network. Although scalability and fairness were not explicitly tested, Nomad has thus far proven to be scalable, as group sizes can be controlled to cater system requirements. In terms of fairness, it can be concluded that as objects are randomly distributed amongst all peers, Nomad is indeed fair.

The Nomad implementation also verified that Pithos' simulated results are accurate and that a Pithos is a suitable storage architecture for P2P MMVEs.

\section{Recommendations for Future Work} \label{sec:future_work}

\begin{enumerate}
  \item \textit{Implement a fully functional certification mechanism} - Nomad currently does not make use of any certification mechanism, which means that peer validation and tracking of object changes are not possible. A certification mechanism should be implemented in order to allow for additional security in Nomad.

  \item \textit{Replace Nomad's overlay storage component} - Nomad currently makes use of TomP2P, a Kademlia based DHT, as overlay storage. During Nomad's evaluation, the overlay storage component was found to be extremely unresponsive under network churn, and generally unreliable. It is therefore recommended to either replace Nomad's overlay component with a more suitable DHT implementation, or to implement a Chord based DHT from scratch.

  \item \textit{Research alternatives for directory server implementation} - Nomad currently makes use of ZooKeeper, for its directory server. In its current state, the directory server is completely functional and capable of providing the required logic. The longevity of ZooKeeper is, however, somewhat of a concern. Projects like \textit{etcd} \cite{etcd} and \textit{consul} \cite{consul} are very promising alternatives that are widely used in the industry and have the prospect of longevity.

  \item \textit{Improve safe storage quorum mechanism} - Nomad's current quorum mechanism relies heavily on Java POJO (Plain Old Java Object) comparison, which is robust, but prone to human error, i.e. if the underlying models are changed without using a framework to generate \textit{GameObjects}. It is recommended to implement a more secure and robust quorum mechanism for Nomad, in order to ensure the correct function of safe retrieval and updates.

  \item \textit{Improve fast storage response times} - In order to make fast storage completely independent of group size, additional software improvements are required. In Nomad's current state, a peer will notify all other group peers of an object added before reporting success. This can be improved by returning storage results \textit{before} notifying peers of new objects. This means that once the object is stored locally, a successful storage result is returned.

  \item \textit{Allow super-peer movements} - To simplify Nomad's Voronoi grouping logic, once a super-peer is selected and it sends its VE location to the directory server, it can no longer receive any positional updates. This means that super-peer positions are static in Nomad's current implementation. Additional logic is required to allow super-peers to move within the VE. This means that groups will have a velocity and that all peers and super-peers need to recalculate their Voronoi maps regularly to ensure a consistent view of the VE. Calculating the Voronoi map too frequently causes excessive system load, especially for large VEs. It is therefore recommended to recalculate the Voronoi map at a reasonable interval.

  \item \textit{Solving NAT challenges} - Docker containers and Kubernetes deploy scripts already exist for Nomad, but in order for Nomad to be used in the cloud, it is required to first solve the problem of NAT traversal. NAT traversal is a problem, due to Nomad's use of random ports for its services, since multiple Nomad instances are executed on a single machine. If static ports are used, the problem will become significantly easier to solve.
\end{enumerate}
